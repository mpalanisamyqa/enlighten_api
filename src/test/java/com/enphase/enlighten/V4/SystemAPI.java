package com.enphase.enlighten.V4;

import com.enphase.enlighten.EnlightenObjects;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.*;

public class SystemAPI implements EnlightenObjects {

    String baseUrl = propertyReader.getConfigProperties(System.getProperty("ENV"),"sys_api_url");
    String basePath = propertyReader.getConfigProperties(System.getProperty("ENV"),"sys_api_path");
    String searchBasePath = propertyReader.getConfigProperties(System.getProperty("ENV"),"sys_search_api_path");
    String userid = propertyReader.getConfigProperties(System.getProperty("ENV"),"userid");
    String key = propertyReader.getConfigProperties(System.getProperty("ENV"),"key");


    @BeforeMethod()
    public void setUp(){
        restUtil.setBaseURI(baseUrl);
        restUtil.setBasePath(basePath);
        restUtil.setContentType(ContentType.JSON);
    }

    @AfterMethod
    public void tearDown(){
        restUtil.resetBaseURI();
        restUtil.resetBasePath();
    }


    public void setHeaders(String status){
        Map<String,Object> headerMap = new HashMap<String, Object>();
        if (status.equalsIgnoreCase("invalid")){
            headerMap.put("KEY","96b79319a4ae6fff4f5eb57700f5c6c");
            headerMap.put("USERID","4d6a45304d5455770a");
        } else {
            headerMap.put("KEY",key);
            headerMap.put("USERID",userid);
        }
        headerMap.put("Content-Type", "application/json");
        restUtil.setHeader(headerMap);
    }


    /*The Below scripts are written for V4 API*/

    /*ENL15662 Story test cases*/

    @Test()
    public void EPA292_verifyStatusCode200(){
        setHeaders("valid");
        Response res = restUtil.getResponse();
        assertHelper.checkStatusIs200(res);
    }

    @Test
    public void EPA292_verifyTotalCount(){
        setHeaders("valid");
        Response res = restUtil.getResponse();
        systemPage.checkSystemListTotal(Integer.parseInt(res.jsonPath().getString("total")));
    }


    @Test
    public void EPA301_verifyStatusCode401(){
        setHeaders("invalid");
        Response res = restUtil.getResponse();
        assertHelper.checkStatusIs401(res);
    }

    @Test
    public void verifyQueryingTwoSystems(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","2");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        assertHelper.checkStatusIs200(res);
        systemPage.verifyTwoSystemsAreDisplayed(Integer.valueOf(res.jsonPath().getString("size")));
        systemPage.verifyTwoSystemsAreDisplayed(Integer.valueOf(res.jsonPath().getString("count")));
    }

    /*ENL10193 and ENL10196 will be covered under this script*/
    @Test
    public void EPA302_verifyStatusCode404(){
        setHeaders("valid");
        Response res = restUtil.getResponse("/6989");
        assertHelper.checkStatusIs404(res);
    }

    @Test
    public void EPA318_verifyStatusCode200(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","name");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        assertHelper.checkStatusIs200(res);
    }

    @Test
    public void EPA318_verifySortOrderAscending(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","name");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        systemPage.verifySortOrderAsAscending(res,"systems","name");
    }

    @Test
    public void EPA322_verifySortOrderAscending(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","id");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        systemPage.verifySortOrderAsAscending(res,"systems","system_id");
    }

    @Test
    public void EPA321_verifySortOrderAscending(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","last_report_date");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        systemPage.verifySortOrderAsAscending(res,"systems","last_report_at");
    }

    @Test
    public void EPA325_verifySortOrderDescending(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","-name");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        systemPage.verifySortOrderAsDescending(res,"systems","name");
    }

    @Test
    public void EPA324_verifySortOrderDescending(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","-id");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        systemPage.verifySortOrderAsDescending(res,"systems","system_id");
    }

    @Test
    public void EPA281_verifySortOrderDescending(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","-last_report_date");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        systemPage.verifySortOrderAsDescending(res,"systems","last_report_at");
    }

    @Test
    public void EPA161_verifyStatusCode422(){
        setHeaders("valid");
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("sort_by","ID");
        restUtil.setQueryParameters(queryMap);
        Response res = restUtil.getResponse();
        assertHelper.checkStatusIs422(res);

    }

    /*ENL15663 Story test cases*/

    @Test
    public void EPA289_verifyStatusCode200(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","2");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10204");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        assertHelper.checkStatusIs200(res);
        systemPage.verifySearchResponse(res,requestBody);

    }

    @Test
    public void EPA294_verifyStatusCode401(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","2");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10205");
        Response res = systemPage.systemSearch("invalid",queryMap,requestBody);
        assertHelper.checkStatusIs401(res);
    }

    @Test
    public void EPA293_verifyStatusCode422(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","2");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10206");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        assertHelper.checkStatusIs422(res);
    }

    @Test
    public void EPA296_verifySystemSizeIsZeroForInvalidSystemSearch(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10207");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        systemPage.verifySystemListTotalAsZero(Integer.parseInt(res.jsonPath().getString("count")));
    }

    @Test
    public void EPA295_verifySortOrderAscending(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10205");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        systemPage.verifySortOrderAsAscending(res,"systems","system_id");
    }

    @Test
    public void EPA298_verifySortOrderAscending(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10209");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        systemPage.verifySortOrderAsAscending(res,"systems","last_report_at");
    }

    @Test
    public void EPA297_verifySortOrderAscending(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","3");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10210");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        systemPage.verifySortOrderAsAscending(res,"systems","name");
    }

    @Test
    public void EPA300_verifySortOrderDescending(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10205");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        systemPage.verifySortOrderAsDescending(res,"systems","system_id");
    }

    @Test
    public void EPA299_verifySortOrderDescending(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10209");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        systemPage.verifySortOrderAsDescending(res,"systems","last_report_at");
    }

    @Test
    public void EPA259_verifySortOrderDescending(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","3");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10210");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        systemPage.verifySortOrderAsDescending(res,"systems","name");
    }

    @Test
    public void EPA272_verifySearchBySystemID(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","enl10215");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        assertHelper.checkStatusIs200(res);
        systemPage.verifyOneSystemsIsDisplayed(Integer.valueOf(res.jsonPath().getString("count")));
        systemPage.verifySearchedSystemID(res.jsonPath().getString("systems.system_id[0]"));
    }

    @Test
    public void EPA276_verifySearchBySystemName(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","epa276");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        assertHelper.checkStatusIs200(res);
        systemPage.verifyOneSystemsIsDisplayed(Integer.valueOf(res.jsonPath().getString("count")));
        systemPage.verifySearchedSystemName(res.jsonPath().getString("systems.name[0]"));
    }

    @Test
    public void EPA275_verifySearchBySystemPublicName(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","epa275");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        assertHelper.checkStatusIs200(res);
        systemPage.verifyOneSystemsIsDisplayed(Integer.valueOf(res.jsonPath().getString("count")));
        systemPage.verifySearchedSystemName(res.jsonPath().getString("systems.name[0]"));
        systemPage.verifySearchedSystemPublicName(res.jsonPath().getString("systems.public_name[0]"));
    }

    @Test
    public void EPA278_verifySearchBySystemStatuses(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","epa278");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        assertHelper.checkStatusIs200(res);
        systemPage.verifyOneSystemsIsDisplayed(Integer.valueOf(res.jsonPath().getString("count")));
        systemPage.verifySearchedSystemName(res.jsonPath().getString("systems.name[0]"));
        systemPage.verifySearchedSystemStatus(res.jsonPath().getString("systems.status[0]"));
    }

    @Test
    public void EPA277_verifySearchBySystemConnectionType(){
        restUtil.setBasePath(searchBasePath);
        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","10");
        queryMap.put("page","1");
        JSONObject requestBody = jsonReader.getJSONObject("SYS_Search_Body.json","epa278");
        Response res = systemPage.systemSearch("valid",queryMap,requestBody);
        assertHelper.checkStatusIs200(res);
        systemPage.verifyOneSystemsIsDisplayed(Integer.valueOf(res.jsonPath().getString("count")));
        systemPage.verifySearchedSystemName(res.jsonPath().getString("systems.name[0]"));
        systemPage.verifySearchedSystemConnectionType(res.jsonPath().getString("systems.connection_type[0]"));
    }

    /*ENL15664 Story test cases*/


    /*ENL15665 Story test cases*/

    /*ENL15666 Story test cases*/





}
