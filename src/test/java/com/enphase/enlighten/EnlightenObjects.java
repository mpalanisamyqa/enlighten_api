package com.enphase.enlighten;

import com.enphase.enlighten.Helpers.AssertHelper;
import com.enphase.enlighten.Helpers.CalendarHelper;
import com.enphase.enlighten.Pages.*;
import com.enphase.enlighten.Utils.JsonReader;
import com.enphase.enlighten.Utils.PropertyReader;
import com.enphase.enlighten.Utils.RestUtil;

public interface EnlightenObjects {
    /*Create Objects for all the Pages and helpers*/

    PropertyReader propertyReader = new PropertyReader();
    RestUtil restUtil = new RestUtil();
    JsonReader jsonReader = new JsonReader();

    /*Helper Objects*/
    AssertHelper assertHelper = new AssertHelper();
    CalendarHelper calendarHelper = new CalendarHelper();

    /*Page Objects*/
    SystemPage systemPage = new SystemPage();
    SendCommandPage sendCommandPage = new SendCommandPage();
    GetStatusPage getStatusPage = new GetStatusPage();
    GetResponsePage getResponsePage = new GetResponsePage();
    GetHistoryPage getHistoryPage = new GetHistoryPage();
    ProvisioningPage provisioningPage = new ProvisioningPage();

}
