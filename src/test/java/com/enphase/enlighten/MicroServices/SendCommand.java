package com.enphase.enlighten.MicroServices;

import com.enphase.enlighten.EnlightenObjects;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SendCommand implements EnlightenObjects {


     String baseUrl = propertyReader.getConfigProperties(System.getenv("ENV"),"cc_url");
     String basePath = propertyReader.getConfigProperties(System.getenv("ENV"),"send_command_path");

    @BeforeMethod
    public void setUp(){
        restUtil.setBaseURI(baseUrl);
        restUtil.setBasePath(basePath);
        restUtil.setContentType(ContentType.JSON);
    }

    @AfterMethod
    public void tearDown(){
        restUtil.resetBaseURI();
        restUtil.resetBasePath();
    }

    @Test
    public void verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","base");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
    }

    @Test
    public void EPA678_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa678");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("gateways",res);
    }

    @Test
    public void EPA679_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa679");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("commands",res);
    }

    @Test
    public void EPA682_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa682");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("command_operation",res);
    }

    @Test
    public void EPA674_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa674");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("abc",res);
    }

    //tbw
//    @Test
    public void EPA681_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa674");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("abc",res);
    }

    //Expired timestamp test case
    @Test
    public void EPA704_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa704");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("issue_time",res);
    }

//    tbw
//    @Test
    public void EPA667_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa704");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("issue_time",res);
    }

    //    tbw
//    @Test
    public void EPA684_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa704");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("issue_time",res);
    }

   // EPA671 & EPA668 are same
   @Test
    public void EPA671_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa671");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
       sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA694_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa694");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("taskId",res);
    }

    // EPA698 & EPA708 are same
    @Test
    public void EPA698_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa698");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA716_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa716");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("mode",res);
    }

    @Test
    public void EPA719_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa719");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA713_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa713");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA665_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa665");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA666_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa666");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA664_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa664");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA663_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa663");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA677_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa677");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

//    EPA662

    @Test
    public void EPA693_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa693");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyPayLoadIsValid(res);
    }

    @Test
    public void EPA717_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa717");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA714_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa714");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA691_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa691");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }

    @Test
    public void EPA706_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa706");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("command_operation_required",res);
    }

    @Test
    public void EPA707_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa707");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        sendCommandPage.verifyERR422Message("gateway",res);
    }

    @Test
    public void EPA669_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa669");
        sendCommandPage.updateIssueTimeAsCurrentGMTTimeStamp(requestBody,calendarHelper.getCurrentGMTTimeStamp());
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        sendCommandPage.verifyRequestIDIsValid(res.jsonPath().getString("request_id"));
    }



}
