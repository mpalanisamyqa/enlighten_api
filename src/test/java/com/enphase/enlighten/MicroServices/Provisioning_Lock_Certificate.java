package com.enphase.enlighten.MicroServices;

import com.enphase.enlighten.EnlightenObjects;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Provisioning_Lock_Certificate implements EnlightenObjects {

    String baseUrl = propertyReader.getConfigProperties(System.getenv("ENV"),"prov_url");
    String basePath = propertyReader.getConfigProperties(System.getenv("ENV"),"prov_lock_certificate_path");

    @BeforeMethod
    public void setUp(){
        restUtil.setBaseURI(baseUrl);
        restUtil.setBasePath(basePath);
        restUtil.setContentType(ContentType.JSON);
    }

    @AfterMethod
    public void tearDown(){
        restUtil.resetBaseURI();
        restUtil.resetBasePath();
    }

    @Test
    public void EPA730_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("Prov_RequestBody.json","epa730");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        provisioningPage.verifySerialNoMissingMessage(res);
    }

    @Test
    public void EPA723_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("Prov_RequestBody.json","epa723");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
    }
}
