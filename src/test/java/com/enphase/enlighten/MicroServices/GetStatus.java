package com.enphase.enlighten.MicroServices;

import com.enphase.enlighten.EnlightenObjects;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GetStatus implements EnlightenObjects {
    String baseUrl = propertyReader.getConfigProperties(System.getenv("ENV"),"cc_url");
    String basePath = propertyReader.getConfigProperties(System.getenv("ENV"),"get_status_path");

    @BeforeMethod
    public void setUp(){
        restUtil.setBaseURI(baseUrl);
        restUtil.setBasePath(basePath);
        restUtil.setContentType(ContentType.JSON);
    }

    @AfterMethod
    public void tearDown(){
        restUtil.resetBaseURI();
        restUtil.resetBasePath();
    }

    // EPA680 & EPA711 & EPA709 & EPA687 are same
    @Test
    public void EPA680_verifyResponseCode400(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa680");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs400(res);
        getStatusPage.verifyInvalidRequestID(res);
    }

    @Test
    public void EPA718_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa718");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        getStatusPage.verifyRequestIdMessage(res,"empty");
    }

    // EPA712 & EPA710 are same
    @Test
    public void EPA712_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa712");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        getStatusPage.verifyExpiredStatus(res);
    }

}
