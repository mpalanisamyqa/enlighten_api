package com.enphase.enlighten.MicroServices;

import com.enphase.enlighten.EnlightenObjects;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Provisioning_Revoke_Certificate implements EnlightenObjects {

    String baseUrl = propertyReader.getConfigProperties(System.getenv("ENV"),"prov_url");
    String basePath = propertyReader.getConfigProperties(System.getenv("ENV"),"prov_revoke_certificate_path");

    @BeforeMethod
    public void setUp(){
        restUtil.setBaseURI(baseUrl);
        restUtil.setBasePath(basePath);
        restUtil.setContentType(ContentType.JSON);
    }

    @AfterMethod
    public void tearDown(){
        restUtil.resetBaseURI();
        restUtil.resetBasePath();
    }

    @Test
    public void EPA955_verifyErrorMessage_CertificateNotGenerated(){
        JSONObject requestBody = jsonReader.getJSONObject("Prov_RequestBody.json","epa955");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        provisioningPage.verifyCertificateNotCreated(res);
    }

    @Test
    public void EPA956_verifyErrorMessage_SerialNoMissing(){
        JSONObject requestBody = jsonReader.getJSONObject("Prov_RequestBody.json","epa956");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        provisioningPage.verifySerialNoMissingMessage(res);
    }

    @Test
    public void EPA953_verifyRevokeCertificate(){
        JSONObject requestBody = jsonReader.getJSONObject("Prov_RequestBody.json","epa953");
        Response res = restUtil.postRequest(requestBody);
        System.out.println(res.asString());
        assertHelper.checkStatusIs200(res);
        provisioningPage.verifyRevokeCertificateMessage(res);
    }


}
