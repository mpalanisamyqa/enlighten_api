package com.enphase.enlighten.MicroServices;

import com.enphase.enlighten.EnlightenObjects;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GetResponse implements EnlightenObjects {

    String baseUrl = propertyReader.getConfigProperties(System.getenv("ENV"),"cc_url");
    String basePath = propertyReader.getConfigProperties(System.getenv("ENV"),"get_response_path");

    @BeforeMethod
    public void setUp(){
        restUtil.setBaseURI(baseUrl);
        restUtil.setBasePath(basePath);
        restUtil.setContentType(ContentType.JSON);
    }

    @AfterMethod
    public void tearDown(){
        restUtil.resetBaseURI();
        restUtil.resetBasePath();
    }

    @Test
    public void EPA673_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa673");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        getResponsePage.verifyRequestIdMessage(res,"empty");

    }

    //EPA675 & EPA685 are same
    @Test
    public void EPA675_verifyResponseCode400(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa675");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs400(res);
        getResponsePage.verifyInvalidRequestID(res);

    }

    @Test
    public void EPA676_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa676");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        getResponsePage.verifyRequestIdMessage(res,"string");

    }

//    tbw
//    EPA670

}
