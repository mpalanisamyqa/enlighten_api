package com.enphase.enlighten.MicroServices;

import com.enphase.enlighten.EnlightenObjects;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class GetHistory implements EnlightenObjects {
    String baseUrl = propertyReader.getConfigProperties(System.getenv("ENV"),"cc_url");
    String basePath = propertyReader.getConfigProperties(System.getenv("ENV"),"get_history_path");

    @BeforeMethod
    public void setUp(){
        restUtil.setBaseURI(baseUrl);
        restUtil.setBasePath(basePath);
        restUtil.setContentType(ContentType.JSON);
    }

    @AfterMethod
    public void tearDown(){
        restUtil.resetBaseURI();
        restUtil.resetBasePath();
    }

    @Test
    public void EPA695_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa695");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        getHistoryPage.verifySerialNoMessage(res,"valid");

    }

    @Test
    public void EPA696_verifyResponseCode200ButEmptyResponse(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa696");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        getHistoryPage.verifyEmptyArrayResponse(res);
    }

    @Test
    public void EPA699_verifyResponseCode422(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa699");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs422(res);
        getHistoryPage.verifySerialNoMessage(res,"number");
    }

    @Test
    public void EPA700_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa700");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
        getHistoryPage.verifySerialNoInResponse(res,getHistoryPage.getSerialNo(res,"serial_number[0]"));
    }

    @Test
    public void EPA692_verifyResponseCode200(){
        JSONObject requestBody = jsonReader.getJSONObject("CC_RequestBody.json","epa692");
        Response res = restUtil.postRequest(requestBody);
        assertHelper.checkStatusIs200(res);
    }


}
