package com.enphase.enlighten;

import static org.junit.Assert.assertTrue;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
        RestAssured.baseURI = "https://api-qa4.enphaseenergy.com";
        RestAssured.basePath = "/api/v4/systems";

        Map<String,Object> headerMap = new HashMap<String, Object>();
        headerMap.put("KEY","96b79319a4ae6fff4f5eb57700f5c6ce");
        headerMap.put("USERID","4d6a45304d5455770a");
        headerMap.put("Content-Type", "application/json");

        Map<String,Object> queryMap = new HashMap<String, Object>();
        queryMap.put("size","2");

        RequestSpecification request = RestAssured.given().headers(headerMap);
//        Response res = request.queryParams(queryMap).get();
        Response res = request.get("/6989");
        System.out.println("sdfsd\n"+ res.asString());
    }
}
