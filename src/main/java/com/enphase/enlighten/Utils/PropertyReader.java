package com.enphase.enlighten.Utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.ini4j.Ini;
import org.ini4j.Profile.Section;

public class PropertyReader {

    public static Section iniLoader (String fileName , String sectype) throws IOException {
        Ini ini = new Ini();
        FileInputStream iniConfigFile = null;
        iniConfigFile = new FileInputStream(
                System.getProperty("user.dir") + File.separator +"src" + File.separator +"test" + File.separator +"resources" + File.separator + fileName + ".properties");
        ini.load(iniConfigFile);
        Section section = ini.get(sectype);
        return section;
    }

    public static String getConfigProperties(String secntype, String val) {
        String value = null;
        try {
            Section section = iniLoader("config",secntype.toUpperCase());
            value = section.get(val);
        } catch (Exception e) {
            System.out.println(e);
        }
        return value;
    }

}

