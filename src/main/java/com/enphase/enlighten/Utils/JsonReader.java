package com.enphase.enlighten.Utils;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

public class JsonReader {


    public JSONObject getJSONObject(String fileName, String enlID) {
        JSONParser parser = new JSONParser();
        JSONObject jsonObject = null;
        JSONObject individualJsonObj;
        Reader reader = null;
        try {
            reader = new FileReader(System.getProperty("user.dir") + File.separator + "src" + File.separator + "test" + File.separator + "resources" + File.separator + "mappings" + File.separator + fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            jsonObject = (JSONObject) parser.parse(reader);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        individualJsonObj = (JSONObject) jsonObject.get(enlID);

        return individualJsonObj;
    }


}
