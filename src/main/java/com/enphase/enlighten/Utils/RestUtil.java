package com.enphase.enlighten.Utils;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.json.simple.JSONObject;

import java.util.Map;

public class RestUtil {
    //Global Setup Variables
    public String path; //Rest request path
    RequestSpecification request;

    /*
    ***Sets Base URI***
    Before starting the test, we should set the RestAssured.baseURI
    */
    public void setBaseURI (String baseURI){
        RestAssured.baseURI = baseURI;
    }

    /*
    ***Sets base path***
    Before starting the test, we should set the RestAssured.basePath
    */
    public void setBasePath(String basePathTerm){
        RestAssured.basePath = basePathTerm;
    }

    /*
    ***Reset Base URI (after test)***
    After the test, we should reset the RestAssured.baseURI
    */
    public void resetBaseURI (){
        RestAssured.baseURI = null;
    }

    /*
    ***Reset base path (after test)***
    After the test, we should reset the RestAssured.basePath
    */
    public void resetBasePath(){
        RestAssured.basePath = null;
    }

    /*
    ***Sets ContentType***
    We should set content type as JSON or XML before starting the test
    */
    public void setContentType (ContentType Type){
        RestAssured.given().contentType(Type);
    }

    /*
    ***search query path of first example***
    It is  equal to "barack obama/videos.json?num_of_videos=4"
    */
    public void  createSearchQueryPath(String searchTerm, String jsonPathTerm, String param, String paramValue) {
        path = searchTerm + "/" + jsonPathTerm + "?" + param + "=" + paramValue;
    }

    public void  createSearchQueryPath(String param, String paramValue) {
        path = "?" + param + "=" + paramValue;
    }


    /*Set headers */
    public void setHeader(Map headers){
        request = RestAssured.given().headers(headers);
    }

    /*Set authentication*/
    public void setAuthentication(Map oauth){
        RestAssured.given().auth();
    }

    /*
    ***Returns response***
    We send "path" as a parameter to the Rest Assured'a "get" method
    and "get" method returns response of API
    */
    public Response getResponse(String path) {
        return request.get(path);
    }

    /*
     ***Returns JsonPath object***
     * First convert the API's response to String type with "asString()" method.
     * Then, send this String formatted json response to the JsonPath class and return the JsonPath
     */
    public JsonPath getJsonPath (Response res) {
        String json = res.asString();
        //System.out.print("returned json: " + json +"\n");
        return new JsonPath(json);
    }


    public Response postRequest(JSONObject requestBody){
        Response response = RestAssured.given().header("Content-Type", "application/json").body(requestBody).when().post().then().extract().response();
        return response;
    }

    /*Get the response of RequestSpecification*/
    public Response getResponse(){
        Response response = request.get();
        return response;
    }

    /*Set the GET query parameters using Map*/
    public void setQueryParameters(Map queryMap){
        request.queryParams(queryMap);
    }


}
