package com.enphase.enlighten.Pages;

import io.restassured.response.Response;
import org.testng.Assert;

public class GetResponsePage {

    /*
    input : response and messageType to be validated
    output : Assert the expected message is available or not
    */

    public void verifyRequestIdMessage(Response response, String messageType) {
        switch (messageType.toLowerCase()) {
            case "empty":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[request_id is not allowed to be empty]", "Expected Request Error message is not displayed");
                break;
            case "string":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[request_id must be a string]", "Expected Request Error message is not displayed");
                break;

                default:
                    Assert.assertTrue(false,"Message mentioned is not available");

        }
    }

    public void verifyInvalidRequestID(Response response){
        Assert.assertEquals(response.jsonPath().get().toString(),"[Invalid request_id]","Expected status message is not displayed");
    }
}
