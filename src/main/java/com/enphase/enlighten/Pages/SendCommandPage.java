package com.enphase.enlighten.Pages;

import com.enphase.enlighten.Utils.RestUtil;
import io.restassured.response.Response;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.util.Iterator;
import java.util.Set;

public class SendCommandPage extends RestUtil {


    public void verifyERR422Message(String requiredText, Response response) {
        switch (requiredText) {
            case "command_operation":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[" + requiredText + " must be one of [schedule]]", "Expected ERR422 message is not displayed");
                break;
            case "abc":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[" + requiredText + " is not allowed]", "Expected ERR422 message is not displayed");
                break;
            case "issue_time":
                Assert.assertTrue((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")).contains("issue_time must be larger than or equal to"), "Expected ERR422 message is not displayed");
                break;
            case "mode":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[" + requiredText + " must be one of [0, 1, 2, 3, 4, 5, 6]]", "Expected ERR422 message is not displayed");
                break;
            case "command_operation_required":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[command_operation is required]", "Expected ERR422 message is not displayed");
                break;
            case "gateway":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[0 must be a number]", "Expected ERR422 message is not displayed");
                break;
            default:
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[" + requiredText + " is required]", "Expected ERR422 message is not displayed");
        }


    }

    /*
    Input : requestid
    Output : verify the request id is alpha numeric and the lenght is 24
    */

    public void verifyRequestIDIsValid(String requestID) {
        Assert.assertTrue(requestID.matches("^[a-zA-Z0-9]*$") && requestID.length() == 24, "Created Request ID is not valid");
    }


    public void updateIssueTimeAsCurrentGMTTimeStamp(JSONObject requestBody, String updatedTimestamp){
        JSONArray jsonArray = (JSONArray) requestBody.get("commands");
        for(int i=0;i<jsonArray.size();i++){

            JSONObject individualElement = (JSONObject) jsonArray.get(i);
            Set allKeys = individualElement.keySet();
            Iterator iterator = allKeys.iterator();
            while(iterator.hasNext()){
                if(iterator.next().toString().equalsIgnoreCase("issue_time")){
                    individualElement.put("issue_time",updatedTimestamp);
                    break;
                }
            }
        }
    }

    public void verifyPayLoadIsValid(Response response) {
        Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")),"[enable must be one of [0, 1, 2]]","Expected error message is not displayed");
    }
}

