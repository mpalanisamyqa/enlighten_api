package com.enphase.enlighten.Pages;

import com.enphase.enlighten.Utils.RestUtil;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.util.Iterator;

public class GetStatusPage extends RestUtil {

    public void verifyInvalidRequestID(Response response){
        Assert.assertEquals(response.jsonPath().get().toString(),"[Invalid request_id]","Expected status message is not displayed");
    }

    /*
    input : response and messageType to be validated
    output : Assert the expected message is available or not
    */

    public void verifyRequestIdMessage(Response response, String messageType) {
        switch (messageType.toLowerCase()) {
            case "empty":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[request_id is not allowed to be empty]", "Expected Request Error message is not displayed");
                break;
            case "string":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[request_id must be a string]", "Expected Request Error message is not displayed");
                break;

            default:
                Assert.assertTrue(false,"Message mentioned is not available");

        }
    }

    public void verifyExpiredStatus(Response response) {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        String serialNumber = null;
        try {
            jsonObject = (JSONObject) jsonParser.parse(response.asString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Iterator iterator = jsonObject.keySet().iterator();
        while (iterator.hasNext()){
            serialNumber = (String) iterator.next();
            break;
        }
        response.jsonPath().get(serialNumber);
    }
}
