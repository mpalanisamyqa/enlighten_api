package com.enphase.enlighten.Pages;

import com.enphase.enlighten.Utils.RestUtil;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;

import java.security.interfaces.RSAKey;
import java.util.Iterator;

public class ProvisioningPage extends RestUtil {

    String certificate_Keysets[] = {"service","mqtt","client"};
    private final String CERTIFICATETIMEOUTMESSAGE = "Certificate of this device has timed out. Make request to get the new certificate.";
    private final String CERTIFICATELOCKEDSUCCESSFULLY = "Certificate locked successfully.";

    public void verifyEnvoyRetiredMessage(Response res) {
        Assert.assertEquals(res.jsonPath().getString("message[0]"),"Envoy already retired","Expected Envoy error message is not displayed");
    }

    public void verifyTheKeySetsInCert(Response res) {
        JSONParser jsonParser = new JSONParser();
        JSONObject jsonObject = null;
        try {
            jsonObject = (JSONObject) jsonParser.parse(res.asString());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Iterator iterator = jsonObject.keySet().iterator();
        Assert.assertEquals(jsonObject.keySet().size(),3,"Expected Certificate Keysets are not available");
        int i = 0;
        while (iterator.hasNext()){
            Assert.assertEquals(iterator.next(),certificate_Keysets[i],"Expected Keyset is not available in the certificate");
            i++;
        }
    }

    public void verifySerialNoMissingMessage(Response res) {
        Assert.assertEquals(res.jsonPath().getString("message"),"Serial number of the device is missing.","Serial No missing error message is not displayed");
    }

    public void verifyCertificateMessages(Response response, String messageType){
        switch (messageType.toLowerCase()){
            case "timeout":
                Assert.assertEquals(response.jsonPath().getString("message"),CERTIFICATETIMEOUTMESSAGE,"Expected timeout error message is not displayed");
                break;
            case "locked":
                Assert.assertEquals(response.jsonPath().getString("message"),CERTIFICATELOCKEDSUCCESSFULLY,"Success message is not displayed");
                break;

        }
    }

    public void verifyCertificateNotCreated(Response res){
        Assert.assertEquals(res.jsonPath().getString("message"),"Certificate is not created for this devise.");
    }

    public void verifyRevokeCertificateMessage(Response response){
        Assert.assertEquals(response.jsonPath().getString("message"),"Certificates deleted successfully.");
    }
}
