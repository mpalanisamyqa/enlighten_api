package com.enphase.enlighten.Pages;

import com.enphase.enlighten.Utils.RestUtil;
import com.google.common.collect.Ordering;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import org.json.simple.JSONObject;
import org.testng.Assert;

import java.util.*;


public class SystemPage extends RestUtil {

    /*Constants*/
    private static final int ADMINUSERSYSTEMLISTTOTAL = 398;
    private static final int SYSTEMSIZEVALUETWO = 2;
    private static final int SYSTEMSIZEZERO = 0;
    private static final int SYSTEMSIZEONE = 1;
    private static final String SEARCHEDSYSTEMID = "698836370";
    private static final String SEARCHEDSYSTEMNAME = "system with PET address";
    private static final String SEARCHEDSYSTEMPUBLICNAME = "Residential System";
    private static final String SEARCHEDSYSTEMSTATUS = "comm";
    private static final String SEARCHEDSYSTEMCONNECTIONTYPE = "ethernet";


    public void checkSystemListTotal(int total) {
        Assert.assertEquals(total, ADMINUSERSYSTEMLISTTOTAL, "System total is wrong!");
    }

    public void verifyTwoSystemsAreDisplayed(int actualSize) {
        Assert.assertEquals(actualSize, SYSTEMSIZEVALUETWO, "System size is not 2!");
    }

    public void verifySortOrderAsAscending(Response res, String jsonPath, String listName) {
        JsonPath jsPath = new JsonPath(res.asString()).setRootPath(jsonPath);
        List<String> listItems = jsPath.getList(listName);
        boolean sorted = Ordering.natural().isOrdered(listItems);
        Assert.assertTrue(sorted, "Provided List is not Sorted!");
    }

    public void verifySortOrderAsDescending(Response res, String jsonPath, String listName) {
        JsonPath jsPath = new JsonPath(res.asString()).setRootPath(jsonPath);
        List<String> listItems = jsPath.getList(listName);
        System.out.println(listItems);
        boolean sorted = Ordering.natural().reverse().isOrdered(listItems);
        Assert.assertTrue(sorted, "Provided List is not Sorted!");
    }

    public Response systemSearch(String headerType, Map parameters, JSONObject requestBody) {
        Response response = RestAssured.given().headers(getHeaders(headerType)).queryParams(parameters).body(requestBody).when().post().then().extract().response();
        return response;

    }

    public Map<String, Object> getHeaders(String status) {
        Map<String, Object> headerMap = new HashMap<String, Object>();
        if (status.equalsIgnoreCase("invalid")) {
            headerMap.put("KEY", "96b79319a4ae6fff4f5eb57700f5c6c");
            headerMap.put("USERID", "4d6a45304d5455770a");
        } else {
            headerMap.put("KEY", "96b79319a4ae6fff4f5eb57700f5c6ce");
            headerMap.put("USERID", "4d6a45304d5455770a");
        }
        headerMap.put("Content-Type", "application/json");
        return headerMap;
    }

    public void verifySearchResponse(Response response, JSONObject jsonBody) {

        ArrayList<String> requestBodyItems = new ArrayList<String>();
        JSONObject jsonObject = (JSONObject) jsonBody.get("system");
        Set<String> keys = jsonObject.keySet();
        Iterator it = keys.iterator();
        while (it.hasNext()) {
            requestBodyItems.add(jsonObject.get(it.next()).toString());
        }

        for (String individualItem : requestBodyItems) {
            int count = 0;
            ArrayList<String> actualResponses = new ArrayList<String>();
            String keyValue = String.valueOf(jsonObject.keySet().toArray()[count]);
            actualResponses.addAll(response.jsonPath().getList("systems" + "." + keyValue));
            Iterator iterator = actualResponses.iterator();
            while (iterator.hasNext()) {
                iterator.next().toString().contains(individualItem);
            }
            count++;
        }

    }

    public void verifySystemListTotalAsZero(int total) {
        Assert.assertEquals(total, SYSTEMSIZEZERO, "System total is wrong!");
    }

    public void verifyOneSystemsIsDisplayed(int total){
        Assert.assertEquals(total, SYSTEMSIZEONE, "System total is wrong!");
    }

    public void verifySearchedSystemID(String systemID){
        Assert.assertEquals(systemID, SEARCHEDSYSTEMID, "System ID is wrong!");
    }

    public void verifySearchedSystemName(String systemName){
        Assert.assertEquals(systemName, SEARCHEDSYSTEMNAME, "System Name is wrong!");
    }

    public void verifySearchedSystemPublicName(String systemPublicName){
        Assert.assertEquals(systemPublicName, SEARCHEDSYSTEMPUBLICNAME, "System Public Name is wrong!");
    }

    public void verifySearchedSystemStatus(String systemStatus){
        Assert.assertEquals(systemStatus, SEARCHEDSYSTEMSTATUS, "System Status is wrong!");
    }

    public void verifySearchedSystemConnectionType(String systemConnectionType){
        Assert.assertEquals(systemConnectionType, SEARCHEDSYSTEMCONNECTIONTYPE, "System ConnectionType is wrong!");
    }


}
