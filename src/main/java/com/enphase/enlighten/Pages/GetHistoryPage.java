package com.enphase.enlighten.Pages;

import com.enphase.enlighten.Utils.RestUtil;
import io.restassured.response.Response;
import org.testng.Assert;

public class GetHistoryPage extends RestUtil {

        /*
    input : response and messageType to be validated
    output : Assert the expected message is available or not
    */

    public void verifySerialNoMessage(Response response, String messageType) {
        switch (messageType.toLowerCase()) {
            case "valid":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[serial_number must be valid]", "Expected Request Error message is not displayed");
                break;
            case "number":
                Assert.assertEquals((response.jsonPath().get().toString().replaceAll("\\\\\"", "").replaceAll("\"", "")), "[serial_number must be a number]", "Expected Request Error message is not displayed");
                break;

            default:
                Assert.assertTrue(false, "Message mentioned is not available");

        }
    }

    public void verifyEmptyArrayResponse(Response response) {
        Assert.assertEquals(response.asString(),"[]","Response Array is not Empty");
    }

    public void verifySerialNoInResponse(Response response, String serialNo){
        Assert.assertEquals(response.jsonPath().getString("serial_number[0]"),serialNo,"Expected "+serialNo+" is not available in the response");
    }

    public String getSerialNo(Response response, String getKeyValue){
        String serialNo = null;
        serialNo = response.jsonPath().get(getKeyValue).toString();
        return serialNo;
    }
}
