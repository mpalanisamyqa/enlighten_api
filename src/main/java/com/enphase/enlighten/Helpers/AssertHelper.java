package com.enphase.enlighten.Helpers;

import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.hamcrest.text.MatchesPattern;
import org.testng.Assert;

public class AssertHelper {

    private static final String MESSAGE422  = "Unprocessable Entity";
    private static final String MESSAGE400  = "Bad Request";
    private static final String MESSAGE401  = "Not Authorized";
    private static final String MESSAGE404  = "Not Found";

    /*
  Verify the http response status returned. Check Status Code is 200?
  We can use Rest Assured library's response's getStatusCode method
  */
    public void checkStatusIs200 (Response res) {
//        AssertJUnit.assertEquals("Status Check Failed!", 200, res.getStatusCode());
        Assert.assertEquals( res.getStatusCode(),200,"Status Check Failed!");
    }

    public void checkStatusIs422 (Response res) {
        Assert.assertEquals( res.getStatusCode(),422,"Status Check Failed!");
//        Assert.assertEquals( res.jsonPath().getString("message"),MESSAGE422,"Status Check Failed!");
    }

    public void checkStatusIs400 (Response res) {
        Assert.assertEquals( res.getStatusCode(),400,"Status Check Failed!");
//        Assert.assertEquals( res.jsonPath().getString("message"),MESSAGE400,"Status Check Failed!");
    }

    public void checkStatusIs401 (Response res) {
        Assert.assertEquals( res.getStatusCode(),401,"Status Check Failed!");
        Assert.assertEquals( res.jsonPath().getString("message"),MESSAGE401,"Status Check Failed!");
    }

    public void checkStatusIs404 (Response res) {
        Assert.assertEquals( res.getStatusCode(),404,"Status Check Failed!");
        Assert.assertEquals( res.jsonPath().getString("message"),MESSAGE404,"Status Check Failed!");
    }

    /*Verify the request id is in alpha numeric format*/
    public void checkRequestIdIsAlphaNumeric(String requestId){
        Assert.assertEquals(requestId, Matchers.matchesPattern("[[:alnum:]]"), "Request Id is Not In Alpha Numeric format");
    }
}
