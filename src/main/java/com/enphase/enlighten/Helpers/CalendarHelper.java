package com.enphase.enlighten.Helpers;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CalendarHelper {

    private final String enlightenCCDateTimeFormat = "EE MMM dd yyyy H:mm:ss";

    public String getCurrentGMTDateTime(){
        DateTimeFormatter format = DateTimeFormatter.ofPattern(enlightenCCDateTimeFormat);
        LocalDateTime localDate = LocalDateTime.now(ZoneId.of("GMT+0000"));
        return localDate.format(format);
    }

    public String getCurrentGMTTimeStamp(){
        LocalDateTime localDate = LocalDateTime.now(ZoneId.of("GMT+0000"));
        ZonedDateTime zonedDateTime = localDate.atZone(ZoneId.of("GMT+0000"));
        long timeStamp = zonedDateTime.toInstant().toEpochMilli();
        return String.valueOf(timeStamp);
    }


}
